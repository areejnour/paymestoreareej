module.exports.routes = {


  'get /index': 'UserController.index',
  'post /register' :  'UserController.register',
  'post /login' : 'UserController.login',
  'post /update' : 'UserController.update',
  'post /getUserData' :'UserController.getUserData'

};
