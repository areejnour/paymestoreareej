import { request } from "https";



var indicative = require('indicative');
class UserController {
     
     
    rules = {
        first_name: 'required|min:2|max:25|regex:^[a-zA-Z0-9]*[a-zA-Z][a-zA-Z0-9]*$',
        last_name: 'required|min:2|max:25|regex:^[a-zA-Z0-9]*[a-zA-Z][a-zA-Z0-9]*$',
        email: 'required|email',
        password: 'required|min:6|max:30'
    }
    message = {
        'first_name.required': 'first name is required',
        'first_name.min': 'first name should be 2 characters or more',
        'first_name.max': 'first name should be 25 characters or less',
        'first_name.regex': 'first name is not valid',

        'last_name.required': 'last name is required',
        'last_name.min': 'last name should be 2 characters or more',
        'last_name.max': 'last name should be 25 characters or less',
        'last_name.regex': 'last name is not valid',

        'email.required': 'email address is required',
        'email.email': 'email address is not valid',

        'password.required': 'password is required',
        'password.min': 'password should be 6 characters or more',
        'password.max': 'password should be 30 characters or less',
    }

    async index(request: Request, response: Response) {
        response.send('I am working');
    }
    return_type(type:string){
        let user:string;
        user = null;
        console.log('refister as '+type);
        if(type === 'seller'){
            
            
            user = 'Seller';
            // console.log(register_as === 'seller');
            console.log(user);
            
            return user;
        }
        else if(type === 'buyer'){
            user= 'Buyer';
            console.log('user '+user);
            return user;
        }
        return user;
    }
    async register(req: Request, res: Response) {
        // await indicative.validateAll(req.body,this.rules,this.message)
        if(req.body.register_as){
            let User = this.return_type(req.body.register_as);      
            global[User].findOne({where:{email:req.body.email}})
            .then((user:any)=>{
                if(user){
                    res.statusCode=300;
                    res.send(User+' is already exists');
                }
                else{
                    global[User].create(req.body)
                    .then((user:any)=>{
                        res.statusCode=200;
                        // req.setHeader('Content-Type','application/json');
                        res.end('Registeration success');
                        // res.json(user);
                    })
                    .catch((err:any)=>{
                        console.log("catch 1\n");
                        
                        console.log(err);
                        
                        res.statusCode=400;
                        // req.setHeader('Content-Type','application/json');
                        res.send('Somting went wrong ');
                    })
                }
    
            })
            .catch((err:any)=>{
                console.log("catch 2\n");
                console.log(err);
                
                res.statusCode=400;
                // req.setHeader('Content-Type','application/json');
                res.send('Somting went wrong ');
            })
        }
        else{
            res.statusCode = 300;
            res.send('please enter user type');
        }
        
        

    }

    async login(req:Request,res:Response){

        if(req.body.login_as){
            let User = this.return_type(req.body.login_as); 

        global[User].findOne({where:{email:req.body.email}})
        .then((user:any)=>{
            if(user && user.password === req.body.password){
                res.statusCode = 200;
                res.send('successfully login');
            }
            else if(user === null){
                res.statusCode = 300;
                res.send('user not found');
            }
        })
        .catch((err:any)=>{
            console.log(err);
            res.statusCode = 400;
            res.send('someting went wrong');
            
        })
    }
    else{
        res.statusCode = 300;
        res.send('please enter user type'); 
    }
    }

    getUserData(req:Request,res:Response){
        if(req.body.login_as){
            let User = this.return_type(req.body.login_as);
            global[User].findOne({where:{email:req.body.email}})
            .then((user:any)=>{
                if(user){
                    res.statusCode=200;
                    res.json(user);
                }
                else{
                    res.statusCode=300;
                    res.send("user not found");
                }
            })
            .catch((err:any)=>{
                res.statusCode = 400;
                res.send('somting went wrong');
            })
        }
    }
    update(req:Request,res:Response){
        if(req.body.login_as){
            let User = this.return_type(req.body.login_as);
            global[User].findOne({where:{email:req.body.email}})
            .then((user:any)=>{
                if(user){
                    user.update({
                        first_name:req.body.first_name,
                        last_name:req.body.last_name,
                        email:req.body.new_mail,
                        password:req.body.password
                    })
                    .then((user:any)=>{
                        res.statusCode=200;
                        res.json(user);
                    })
                    .catch((err:any)=>{
                        res.statusCode = 400;
                        res.send('somting went wrong');
                    })
                }
                else{
                    res.statusCode=300;
                    res.send("user not found");
                }
            })
            .catch((err:any)=>{
                res.statusCode = 400;
                res.send('somting went wrong');
            })
        }
    }


}

module.exports = new UserController();